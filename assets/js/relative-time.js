import $ from 'jquery';
import { distanceInWords, distanceInWordsToNow, isBefore } from 'date-fns';
import Translator from 'bazinga-translator';

function makeTimesRelative(locale) {
    $('.js-relative-time[datetime]').each(function () {
        const isoTime = $(this).attr('datetime');

        $(this).text(distanceInWordsToNow(isoTime, {
            addSuffix: true,
            locale: locale,
        }));
    });

    $('.js-relative-time-diff[datetime][data-compare-to]').each(function () {
        const timeA = $(this).attr('datetime');
        const timeB = $(this).data('compare-to');

        const relativeTime = distanceInWords(timeA, timeB, { locale: locale });

        const format = isBefore(timeB, timeA)
            ? 'time.later_format'
            : 'time.earlier_format';

        $(this).text(Translator.trans(format, { relative_time: relativeTime }));
    });
}

function loadLocaleAndMakeTimesRelative(lang) {
    import(`date-fns/locale/${lang}/index.js`).then(locale => {
        makeTimesRelative(locale);
    }).catch(error => {
        const i = lang.indexOf('-');

        if (i !== -1) {
            const newLang = lang.substring(0, i);

            console && console.log(`Couldn't load ${lang}; trying ${newLang}`);

            loadLocaleAndMakeTimesRelative(newLang);
        } else {
            console && console.log(error.toString());

            // give up and just do english
            makeTimesRelative();
        }
    });
}

const locale = $(':root').attr('lang');

if (!locale || locale === 'en') {
    // english is the default, always-loaded locale
    makeTimesRelative();
} else {
    loadLocaleAndMakeTimesRelative(locale);
}
